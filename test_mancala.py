from mancala import move
from mancala import starting_state
from mancala import initial_board
from mancala import MancalaState

def test_player_1_picks_bucket_0():
    assert move(player=1, bucket=0).board
    assert move(player=1, bucket=0).player == 2

def test_player_1_picks_bucket_0_with_state():
    assert move(player=1, bucket=0, state=starting_state)

def test_player_2_picks_bucket_0():
    assert move(player=2, bucket=0) == False

def test_players_picks_bucket_6():
    assert move(player=1, bucket=6) == False
    assert move(player=2, bucket=6) == False

def test_player_1_picks_bucket_2_still_has_turn():
    assert move(player=1, bucket=2).player == 1


def test_players_picks_bucket_13():
    assert move(player=1, bucket=13) == False
    assert move(player=2, bucket=13) == False

def test_player_2_picks_bucket_7():
    assert move(player=2, bucket=7).board[1] == 4

def test_bucket_11_has_four_stones():
    assert move(player=1, bucket=0).board[11] == 4

def test_player_1_picks_bucket_0_then_bucket_0_should_be_empty():
    assert move(player=1, bucket=0).board[0] == 0

def test_player_2_picks_bucket_7_then_bucket_7_should_be_empty():
    assert move(player=2, bucket=7).board[7] == 0

def test_player_1_picks_bucket_0_then_bucket_1_should_have_5():
    assert move(player=1, bucket=0).board[1] == 5

def test_player_2_picks_bucket_12_then_bucket_0_should_have_5():
    assert move(player=2, bucket=12).board[0] == 5

def test_player_2_picks_bucket_9_then_bucket_13_should_have_1():
    assert move(player=2, bucket=9).board[13] == 1

def test_wrap_around_skip_other_players_mancala():
    state = MancalaState(initial_board, 1)
    state.board[5] = 8
    assert move(player = 1, bucket = 5).board[13] == 0
