class MancalaState:
    def __init__(self, board, player):
        self.board = board
        self.player = player

initial_board = [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0]
starting_state = MancalaState(initial_board, 1)

def move(player=1, bucket=0, state=None):
    if state is None:
        state = starting_state
    def make_move():
        new_state = MancalaState(list(state.board), player)
        bucket_value = new_state.board[bucket]
        new_state.board[bucket] = 0
        b = bucket+1
        print(f"starting b = {b}")
        # Some kind of fencepost error!
        while b < bucket+1+bucket_value:
            print(f"b = {b}")
            new_state.board[b % 14] += 1
            b += 1
        # for b in range(bucket+1, bucket+1+bucket_value):
        #     new_state.board[b % 14] += 1
        if b not in (6, 13):
            new_state.player = 2 if player == 1 else 1
        return new_state

    if player == 1 and bucket in range(0, 6):
        return make_move()
    elif player == 2 and bucket in range(7, 13):
        return make_move()
    return False
